sel_ms2_files<-function(ftable,dirMS2) {
  patt<-function(id,mode,tag,set) {
    paste('0*',id,'_',mode,'_',tag,'_',set,'.csv',sep='')
  }

  gen_ms2_spec_fn <- function(id,tag,mode,set,width=6) {
    suppressWarnings({
        iid<-as.numeric(id)
        iid<- if (!is.na(iid)) iid else id
        num <- formatC(iid,width = width,format='d',flag='0')
        ss<-trimws(paste(num,mode,tag,set,sep="_"),which='both')
        paste(ss,".csv",sep='')
    })
  }
  filt_state <- ftable[ftable$MS1 & ftable$MS2 & ftable$Alignment & ftable$AboveNoise & !is.na(ftable$MS2rt),]
  ptrns <- patt(filt_state$ID,
                filt_state$mode,
                filt_state$tag,
                filt_state$set)
  ## allfiles<-list.files(path=dirMS2,full.names = T)
  thefiles <- apply(filt_state,1,function(row) file.path(dirMS2,gen_ms2_spec_fn(row['ID'],
                                                                                row['tag'],
                                                                                row['mode'],
                                                                                row['set'])))
  
  
  filt_state$MS2File<-thefiles
  filt_state
}

file2tab <- function(...) read.csv(...,stringsAsFactors = F)




